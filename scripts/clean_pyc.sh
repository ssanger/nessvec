pkg_name=nessvec
full_path=$(realpath $0)
echo $0
dir_path=$(dirname $full_path)
echo "Running $full_path on dir $dir_path..."
set -e
echo 'find "'$dir_path'"/.. -name '"*.pyc"' -exec rm -rf "{}" \;'
# find "$dir_path"/.. -name '*.egg-info' -exec rm -rf "{}" \;
# rm -rf "$dir_path"/../build
# rm -rf "$dir_path"/../.eggs

rm -rf src/$pkg_name.egg-info/
rm -rf .eggs/ dist build
find . -name '*.egg-info' -exec rm -rf "{}" \;
find . -name '*.pyc' -exec rm -rf "{}" \;
