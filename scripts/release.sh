#!/usr/bin/env bash
# release.sh
USAGE="./release.sh [MESSAGE [| VERSION]]"
DEPENDENCIES=(twine wheel setuptools pip keyring rfc3986)

PKG_NAME=nessvec
DATA_DIR=src/$PKG_NAME/data
VERSION=$1
MESSAGE=$2


# set -e

if [ -z "$1" ] || [ -z "$2" ] ; then
    echo "USAGE: ./scripts/release.sh VERSION MESSAGE"
    echo ""
    echo "EXAMPLE: ./scripts/release.sh 1.2.3 'add new $PKG_NAME feature'"
    exit 1
else
    echo "Recent versions: "
    git tag | sort | tail -n 10
    echo ""
    echo "Tagging the $PKG_NAME git repository with: git tag -a '$VERSION' -m '$MESSAGE'..."
    echo "Do you want to proceed [N]/y?"
fi

read answer

if [ "$answer" != "y" ] ; then
    exit 0
fi

set -e
# pip install -U twine wheel setuptools
git commit -am "$1: $2"
git push
git tag -l | cat

rm -rf build
rm -rf dist

echo "Large data files..."
find src/$PKG_NAME/data -type f -size +20M -exec ls -hal {} \;
echo ""
echo "Would you like to remove these files?"
read answer
if [ "$answer" == "y" ] ; then
    find src/$PKG_NAME/data -type f -size +20M -exec rm -f {} \;
fi

# python setup.py sdist
# python setup.py bdist_wheel

# if [ -z "$(which twine)" ] ; then
#     echo 'Unable to find `twine` so installing it with pip.'
#     pip install --upgrade pip setuptools twine poetry
# fi


sed -i 's/^version\s*=\s*"[0-9]*\.[0-9]*\.[0-9]*"/version = "'$1'"/g' scripts/pyproject.toml
grep '^version' scripts/pyproject.toml
git commit -am "$1: $2"



function upload_with_twine {

    CMD=$'try:\n  import twine\n  print("1")\nexcept ImportError:\n  print("")\n\n\n'
    TWINE_INSTALLED=$(python -c "$CMD")
    if [ -n "$TWINE_INSTALLED" ] ; then
       echo "twine is already installed"
    else
       echo "Installing dependencies: ${DEPENDENCIES[*]}"
       conda install -c conda-forge -c defaults -y ${DEPENDENCIES[*]}
       # pip install --upgrade twine wheel setuptools
    fi

    rm -rf build
    rm -rf dist
    # find nessvec-data -type f -size +10M -exec rm -f {} \;

    python setup.py sdist bdist_wheel

    if [ -z "$(which twine)" ] ; then
        echo 'Unable to find `twine` so installing twine with conda.'
        conda install -c conda-forge -c defaults -y twine
    fi

    twine check dist/*
    twine upload dist/"$PKG_NAME-$VERSION"*"-py"* --verbose
    twine upload dist/"$PKG_NAME-$VERSION"*".tar"* --verbose
    git push --tag
}


function upload_with_poetry {
    cp scripts/pyproject.toml .
    
    # poetry config pypi-token.pypi "$PYPI__TOKEN__"  # probably need to do this fir
    poetry build
    poetry publish  # poetry publish --username=__token__ --password=pypi-...

    rm pyproject.toml

    git commit -am "pyproject file version updated for release $VERSION: $MESSAGE"
    git push
    git tag -a "$VERSION" -m "$MESSAGE"
    git push --tag
}


upload_with_twine